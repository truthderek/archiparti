import 'perfect-scrollbar/src/js/adaptor/jquery';
import 'moment/locale/zh-cn';
import 'moment/locale/en-gb';
import 'moment-timezone/builds/moment-timezone-with-data';
import 'bootstrap/js/modal';
import 'slick-carousel';
import 'jquery.maskedinput/src/jquery.maskedinput';
import 'select2/dist/js/select2.full.min';

import angular from 'angular';
import 'angular-perfect-scrollbar';
import 'angular-ui-router';
import 'angular-file-upload';
import 'angular-sanitize';

angular.module('Vendor', [
    'ui.router',
    'ngSanitize',
    'perfect_scrollbar',
    'angularFileUpload',
]);