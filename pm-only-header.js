import HeaderDropdown from './components/header-dropdown/header-dropdown.component';
import Avatar from './components/avatar/avatar.component';

import jsMenu from './directives/js-menu.directive';
import locale from './directives/locale.directive';
import localeAttr from './directives/locale-attr.directive';

import ProjectEventFeedService from './services/project/feed.service';
import ProjectEventService from './services/project/event.service';
import ProjectService from './services/project/project.service';
import SocketService from './services/socket/socket.service';
import LocaleService from './services/locale/locale.service';
import LocaleLinkService from './services/locale/link.service';

import angular from 'angular';

angular.module('app', [])
    .component('headerDropdown', HeaderDropdown)
    .component('avatar', Avatar)

    .directive('jsMenu', jsMenu)
    .directive('locale', locale)
    .directive('localeAttr', localeAttr)

    .service('SocketService', SocketService)
    .service('ProjectService', ProjectService)
    .service('ProjectEventService', ProjectEventService)
    .service('ProjectEventFeedService', ProjectEventFeedService)
    .service('LocaleService', LocaleService)
    .service('LocaleLinkService', LocaleLinkService);

angular.injector(['ng', 'app'])
    .get('ProjectEventFeedService')
.openSocket();
