angular.module('app')
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push(function() {
            return {
                response: function(response) {
                    if (response.data.redirect) {
                        window.location = response.data.redirect;
                    }

                    return response;
                }
            };
        });
    }]);