class ProjectDesignerController {
    static $inject = [
        'ProjectDesignerService',
        'ProjectEventService',
        '$state'
    ];

    constructor(ProjectDesignerService, ProjectEventService, $state) {
        ProjectEventService.subscribe(this);

        this.ProjectDesignerService = ProjectDesignerService;
        this.ProjectEventService = ProjectEventService;
        this.state = $state;
    }

    $onInit() {
        this.taskEvent = this.ProjectEventService.types.taskstatus;
        this.projectId = this.state.params.projectId;
        this.designers = [];

        this.loadDesigners();
    }

    canKill() {
        return (this.projectId != this.state.params.projectId);
    }

    notify(jsonEvent) {
        let event = JSON.parse(jsonEvent);

        if (event.type == this.taskEvent) {
            this.tryRefreshProgress(event.task);
        }
    }

    tryRefreshProgress(task) {
        let designerIds = this.designers.map(designer => designer.id);

        if (designerIds.includes(task.user_id)) {
            this.loadProgress();
        }
    }

    loadDesigners() {
        this.ProjectDesignerService.list(this.projectId)
            .then(response => {
                this.appendDesigners(response.data.designers);
                this.loadProgress();
                this.attemptRedirect();
            });
    }

    loadProgress() {
        this.ProjectDesignerService.progress(
                this.projectId,
                this.designers.map(designer => designer.id)
            )
            .then(response => this.progress = response.data.progress);
    }

    appendDesigners(designers) {
        this.designers = designers;
        this.ProjectDesignerService.designersCount = this.designers.length;
    }

    attemptRedirect() {
        if (this.designers.length == 1 && !this.state.params.taskId) {
            this.state.go('pm.project.show.designer', {
                designerId: this.designers[0].id
            });
        }
    }
}

export default ProjectDesignerController;