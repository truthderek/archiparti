class ProjectTaskController {
    static $inject = [
        'ProjectEventFeedService',
        'ProjectEventService',
        'ProjectTaskService',
        'ProjectService',
        'ProjectDesignerService',
        '$state'
    ];

    constructor(ProjectEventFeedService, ProjectEventService, ProjectTaskService, ProjectService, ProjectDesignerService, $state) {
        ProjectTaskService.subscribe(this);
        ProjectEventFeedService.subscribe(this);

        this.ProjectService = ProjectService;
        this.ProjectTaskService = ProjectTaskService;
        this.ProjectEventService = ProjectEventService;
        this.ProjectDesignerService = ProjectDesignerService;
        this.state = $state;
    }

    $onInit() {
        this.hideList = [];
        this.taskEvent = this.ProjectEventService.types.taskstatus;
        this.taskStatuses = this.ProjectTaskService.statuses;
        this.projectId = this.state.params.projectId;
        this.designerId = this.state.params.designerId;

        this.status(this.taskStatuses.progress.index);
        this.getProject();
    }

    canKill() {
        let designerChanged = (this.state.params.designerId != this.designerId);
        let projectChanged = (this.state.params.projectId != this.projectId);

        return (designerChanged || projectChanged);
    }

    notifyTaskStatus(taskId, newStatus) {
        if (this.currentStatus != newStatus) {
            this.hideList[taskId] = true;
        }
    }

    notify(jsonEvent) {
        let event = JSON.parse(jsonEvent);

        this.attachAssignedToTask(event);
        this.applyTask(event);
    }

    attachAssignedToTask(event) {
        event.task.assigned = event.receiver.find(user => user.id == event.task.assigned_id);
    }

    applyTask(event) {
        let isTaskEvent = (event.type == this.taskEvent);
        let inProject = (event.project.id == this.projectId);

        if (inProject && isTaskEvent) {
            this.hideOrShowTask(event.task);
        }
    }

    hideOrShowTask(task) {
        if (task.status == this.currentStatus) {
            this.hideList[task.id] = false;
            this.appendTask(task);
        } else {
            this.hideList[task.id] = true;
        }
    }

    appendTask(task) {
        for (let index in this.taskLists) {
            let list = this.taskLists[index];

            if (this.isElementOfTree(task, list)) {
                list.children = list.children.filter(listTask => listTask.id != task.id).push(task);
            }
        }
    }

    isElementOfTree(task, list) {
        return (list.lft < task.lft && list.rgt > task.rgt);
    }

    status(status) {
        this.currentStatus = status;
        this.loading = true;
        this.list();
    }

    list() {
        this.ProjectTaskService.list(this.currentStatus)
            .then(response => {
                this.taskLists = response.data.tasks;
                this.ProjectTaskService.setCurrentTasks(this.taskLists);
                this.hideList = [];
                this.loading = false;
            });
    }

    getProject() {
        let activeProject = this.ProjectService.activeProject;

        if (activeProject && activeProject.id == this.projectId) {
            this.project = this.ProjectService.activeProject;
            this.ProjectService.setActiveProject(this.ProjectService.activeProject);
        } else {
            this.loadProject();
        }
    }

    loadProject() {
        this.ProjectService.show(this.projectId)
            .then(response => {
                this.project = response.data.project;
                this.ProjectService.setActiveProject(response.data.project);
            });
    }
}

export default ProjectTaskController;