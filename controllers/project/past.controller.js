class ProjectPastController {
    static $inject = [
        'ProjectService',
        'ProjectDesignerService',
        'AuthService',
        '$state'
    ];

    constructor(ProjectService, ProjectDesignerService, AuthService, $state) {
        this.ProjectService = ProjectService;
        this.ProjectDesignerService = ProjectDesignerService;
        this.AuthService = AuthService;
        this.state = $state;
    }

    $onInit() {
        this.ProjectDesignerService.designersCount = null;
        this.getUser();
        this.list();
    }

    list() {
        this.loading = true;

        this.ProjectService.past()
            .then(response => {
                this.projects = response.data.projects;
                this.loading = false;
            });
    }

    getUser() {
        this.AuthService.get()
            .then(response => this.user = response.data.user);
    }

    inTodo(project) {
        return (project.status == this.ProjectService.statuses.todo);
    }

    resume(project) {
        this.ProjectService.resume(project.id)
            .then(response => this.removeProject(project.id));
    }

    removeProject(projectId) {
        this.projects = this.projects.filter(project => project.id != projectId);
    }

    formatDate(date) {
        return date ? moment(date).format('D MMM, h:mm a') : '';
    }
}

export default ProjectPastController;