class ProjectCreateController {
    static $inject = [
        'ProjectService',
        '$state'
    ];

    constructor(ProjectService, $state) {
        this.ProjectService = ProjectService;
        this.$state = $state;
        this.project = {color: '#7f7d7d'};
    }

    submit() {
        this.ProjectService.save(this.project)
            .then(response => {
                if (response.data.id) {
                    this.$state.go('pm.project.show', {projectId: response.data.id});
                }

                this.errors = response.data.errors;
            });
    }
}

export default ProjectCreateController;