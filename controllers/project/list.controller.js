class ProjectListController {
    static $inject = [
        'ProjectEventFeedService',
        'ProjectEventService',
        'ProjectDesignerService',
        'ProjectContractService',
        'ProjectService',
        'AuthService',
        '$state'
    ];

    constructor(
        ProjectEventFeedService,
        ProjectEventService,
        ProjectDesignerService,
        ProjectContractService,
        ProjectService,
        AuthService,
        $state
    ) {
        this.ProjectService = ProjectService;
        this.ProjectEventService = ProjectEventService;
        this.ProjectEventFeedService = ProjectEventFeedService;
        this.ProjectDesignerService = ProjectDesignerService;
        this.ProjectContractService = ProjectContractService;
        this.AuthService = AuthService;
        this.state = $state;
    }

    $onInit() {
        this.setLocalVariables();
        this.fetchData();
    }

    setLocalVariables() {
        this.page = 0;
        this.user = {};
        this.projects = [];
        this.counters = [];
        this.hideList = [];
        this.statuses = this.ProjectService.statuses;
        this.taskEvent = this.ProjectEventService.types.taskstatus;
    }

    fetchData() {
        this.ProjectEventFeedService.subscribe(this);

        this.loadUser();
        this.loadCounters();
        this.list();
    }

    canKill() {
        return false;
    }

    notify(jsonEvent) {
        let event = JSON.parse(jsonEvent);

        if (event.type == this.taskEvent) {
            this.refreshProject(event.project);
        }
    }

    refreshProject(newProject) {
        for (let project of this.projects) {
            if (project.id == newProject.id) {
                this.updateProgress(project, newProject);
            }
        }
    }

    updateProgress(oldProject, newProject) {
        if (newProject.status == this.statuses.done) {
            this.hideList[newProject.id] = true;
            this.loadCounters();
        } else {
            this.updateProjectProgress(oldProject, newProject.progress);
            this.updateDesignerProgress(oldProject, newProject.progress);
        }
    }

    updateProjectProgress(project, progress) {
        project.progress = progress;
    }

    updateDesignerProgress(project, progress) {
        project.designer.forEach((designer) => {
            if (designer.id == this.user.id) {
                designer.pivot.progress = progress;
            }
        });
    }

    list() {
        this.ProjectService.list(++this.page)
            .then(response => {
                this.projects.push(...response.data.projects.data);
                this.contracts = response.data.contracts;
            });
    }

    loadUser() {
        this.AuthService.get().then(response => this.user = response.data.user);
    }

    loadCounters() {
        this.ProjectService.counters()
            .then(response => this.counters = response.data.counters);
    }

    hasProjectsWithStatus(status) {
        return this.counters.reduce(function (reduced, counter) {
            return reduced || (counter.status == status && counter.count > 0 && !counter.is_archived);
        }, false);
    }

    hasArchivedProjects() {
        return this.counters.reduce(function (reduced, counter) {
            return reduced || counter.is_archived;
        }, false);
    }

    getProgress(project) {
        if ((project.owner_id == this.user.id) || this.user.is_admin) {
            return project.progress;
        } else {
            return this.findSelfInProject(project).pivot.progress;
        }
    }

    getDesignersCount(project) {
        if (project.owner_id == this.user.id || this.user.is_admin) {
            return project.designer.length;
        } else {
            return [this.findSelfInProject(project)].length;
        }
    }

    findSelfInProject(project) {
        return project.designer.find(designer => designer.id == this.user.id);
    }

    chooseProject(project) {
        if (this.state.params.projectId == project.id) return;

        this.ProjectService.activeProject = project;
        this.ProjectContractService.contracts[project.id] = this.getContracts(project);
        this.ProjectDesignerService.designers[project.id] = project.designer;

        this.state.go('pm.project.show', {projectId: project.id});
    }

    getContracts(project) {
        return this.contracts.filter(contract => contract.project_id == project.id);
    }

    getType(project) {
        return project.type ? project.type.name : '';
    }

    formatDate(date) {
        return moment(date).format('D MMM, h:mm a');
    }
}

export default ProjectListController;
