class ProjectDiaryController {
    static $inject = [
        '$state',
        'ProjectEventService',
        'ProjectEventFeedService'
    ];

    constructor($state, ProjectEventService, ProjectEventFeedService) {
        this.ProjectEventService = ProjectEventService;
        this.state = $state;

        ProjectEventFeedService.subscribe(this);

        this.page = 0;
        this.events = [];
        this.projectId = $state.params.projectId;

        this.list();
    }

    list() {
        this.ProjectEventService.list(++this.page, this.projectId)
            .then(response => {
                this.events = $.merge(this.events, response.data.events.data);
            });
    }

    readEvent(event) {
        this.ProjectEventService.readByEvents([event.id]);
    }

    canKill() {
        return (this.projectId != this.state.params.projectId);
    }

    notify(jsonEvent) {
        let event = JSON.parse(jsonEvent);

        if (this.projectId == event.project_id) {
            this.attachEvent(event);
        }
    }

    attachEvent(event) {
        this.events.unshift(event);

        $('.b-project-list--project-diary').animate({scrollTop: 0}, 1000);
        $('.b-project-list--project-diary').perfectScrollbar('update');
    }

    formatDate(date) {
        return moment(date).fromNow();
    }
}

export default ProjectDiaryController;