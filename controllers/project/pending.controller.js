class ProjectPendingController {
    static $inject = [
        'ProjectService',
        'ProjectInviteService',
        'ProjectDesignerService',
        'AuthService',
        'ConfigService',
        '$window'
    ];

    constructor(ProjectService, ProjectInviteService, ProjectDesignerService, AuthService, ConfigService, $window) {
        this.ProjectService = ProjectService;
        this.ProjectInviteService = ProjectInviteService;
        this.ProjectDesignerService = ProjectDesignerService;
        this.AuthService = AuthService;
        this.ConfigService = ConfigService;
        this.$window = $window;
    }

    $onInit() {
        this.ProjectDesignerService.designersCount = null;
        this.getUser();
        this.list();
        this.configList();
    }

    list() {
        this.loading = true;

        this.ProjectService.pending()
            .then(response => this.loadProjects(response.data.projects));
    }

    configList() {
        this.ConfigService.list()
            .then(response => {
                this.ownernotifyminutes = response.data.settings.find(setting => setting.const === this.ConfigService.consts.ownernotifyminutes).data;
                this.timezone = response.data.timezone;
            });
    }

    loadProjects(projects) {
        this.loadOwnedProjects(projects);
        this.loadInvitedProjects(projects);
        this.loading = false;
    }

    loadOwnedProjects(projects) {
        let ownedProjects = projects.filter(project => project.owner_id == this.user.id);

        this.ownedProjectsTodo = ownedProjects.filter(project => {
            return project.status == this.ProjectService.statuses.todo;
        });
        this.ownedProjectsPremoderation = ownedProjects.filter(project => {
            return project.status == this.ProjectService.statuses.premoderation;
        });
    }

    loadInvitedProjects(projects) {
        let invitedProjects = this.getInvitedProjects(projects);

        this.invitedProjectsWithOffer = invitedProjects.filter(project => this.hasOffer(project));
        this.invitedProjectsWithoutOffer = invitedProjects.filter(project => !this.hasOffer(project));
    }

    getInvitedProjects(projects) {
        return projects.filter(project => project.owner_id != this.user.id && !project.invite[0].is_outdated);
    }

    getUser() {
        this.AuthService.get().then(response => this.user = response.data.user);
    }

    decline(projectId) {
        this.ProjectInviteService.decline(this.getInviteId(projectId))
            .then(response => this.removeProject(projectId));
    }

    getInviteId(projectId) {
        let project = this.invitedProjectsWithoutOffer.find(project => project.id == projectId);

        if (!project) return;

        return project.invite[0].id;
    }

    removeProject(projectId) {
        this.invitedProjectsWithoutOffer = this.invitedProjectsWithoutOffer.filter(project => project.id != projectId);
        this.invitedProjectsWithOffer = this.invitedProjectsWithOffer.filter(project => project.id != projectId);
    }

    getStartDate(project) {
        return this.formatDate(project.can_start_at);
    }

    getTillDate(project) {
        return this.formatDate(moment(project.created_at).add(this.ownernotifyminutes, 'minutes'));
    }

    formatDate(date) {
        return date ? moment(date).format('D MMM, h:mm a') : '';
    }

    isTermCollectionExpired(project) {
        return moment().tz(this.timezone).format('D MMM, h:mm a') > this.getTillDate(project);
    }

    redirectToInvite(project) {
        if (this.hasOffer(project) || this.inviteIsOutdated(project)) return;

        this.$window.location = '/project/invite/' + project.id;
    }

    inviteIsOutdated(project) {
        return project.invite[0].is_outdated;
    }

    hasOffer(project) {
        if (!project.invite.length) return;

        return project.invite[0].offer_count;
    }

    redirectToProjectChoose(project) {
        this.$window.location = '/project/' + project.id + '/choose';
    }
}

export default ProjectPendingController;
