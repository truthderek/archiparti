class ProjectChatController {
    static $inject = [
        'ChatService',
        'ProjectTaskService',
        'ProjectEventService',
        'AuthService',
        'SocketService',
        'FileUploader',
        '$stateParams',
        '$timeout',
        '$sanitize'
    ];

    constructor(ChatService, ProjectTaskService, ProjectEventService, AuthService, SocketService, FileUploader, $stateParams, $timeout, $sanitize) {
        this.ChatService = ChatService;
        this.ProjectTaskService = ProjectTaskService;
        this.ProjectEventService = ProjectEventService;
        this.AuthService = AuthService;
        this.SocketService = SocketService;
        this.FileUploader = new FileUploader;
        this.$timeout = $timeout;
        this.sanitize = $sanitize

        this.ProjectTaskService.subscribe(this);

        this.taskId = $stateParams.taskId;
        this.messages = [];
        this.files = [];
        this.page = 0;

        this.initController();
    }

    initController() {
        this.getTask();
        this.getUser();
        this.getFiles();
        this.openSocket();
        this.lastMessages();
        this.prepareUploader();
    }

    notifyTaskStatus(taskId, newStatus) {
        let review = this.ProjectTaskService.statuses.review.index;
        let isActive = (this.ProjectTaskService.activeTask.id == taskId);

        if (isActive && (newStatus == review)) {
            this.hideControls = false;
        }
    }

    lastMessages() {
        this.ChatService.lastMessages(this.taskId, ++this.page)
            .then(response => {
                this.mergeAndSortMessages(response.data.messages.data);
                this.readMessagesEvents(response.data.messages.data);
            });
    }

    mergeAndSortMessages(messages) {
        this.messages = $.merge(
            messages.sort(function(a, b) {
                return a.id - b.id;
            }),
            this.messages
        );
    }

    openSocket() {
        this.SocketService.open('chat?id=' + this.taskId, {
            onmessage: angular.bind(this, this.onMessage)
        });
    }

    onMessage(socketMessage) {
        let message = JSON.parse(socketMessage.data);
        this.messages.push(message);

        this.readMessagesEvents([message]);
        this.getFiles();
        this.scrollDown();
    }

    readMessagesEvents(messages) {
        this.ProjectEventService.readByMessages(messages.map(message => message.id));
    }

    send() {
        if (this.formDisabled) return;
        this.ChatService.send({text: this.text, task_id: +this.taskId, files: this.files})
            .then(response => {
                this.SocketService.send('chat?id=' + this.taskId, response.data.message);
                this.error = false;
            }, response => {
                this.error = response.data.message;
                this.scrollDown();
            });

        this.text = null;
        this.files = [];
        this.FileUploader.clearQueue();
    }

    scrollDown() {
        this.$timeout(function() {
            $('.b-comment.js-scroll-block').animate({
                scrollTop: $('.b-comment.js-scroll-block').prop('scrollHeight')
            });
        });
    }

    formatDate(date) {
        date = date || moment();

        return moment(date).format('D MMM, h:mm a');
    }

    getUser() {
        this.AuthService.get()
            .then(response => {
                this.user = response.data.user;
            });
    }

    getTask() {
        if (this.ProjectTaskService.activeTask) {
            this.setUploaderUrl();
        } else {
            this.ProjectTaskService.show(this.taskId)
                .then(response => {
                    this.ProjectTaskService.activeTask = response.data.task;
                    this.setUploaderUrl();
                });
        }
    }

    prepareUploader() {
        this.FileUploader.autoUpload = true;
        this.FileUploader.onBeforeUploadItem = angular.bind(this, this.disableForm);
        this.FileUploader.onSuccessItem = angular.bind(this, this.onUploaded);
    }

    setUploaderUrl() {
        this.FileUploader.url = this.ChatService.uploadUrl(
            this.ProjectTaskService.activeTask.id
        );
    }

    getFiles() {
        this.ChatService.files(this.taskId);
    }

    canDecline() {
        return this.ProjectTaskService.activeTask.can_decline;
    }

    canReview() {
        let activeTask = this.ProjectTaskService.activeTask || {};

        return ((activeTask.can_decline || activeTask.can_approve) && !this.hideControls);
    }

    approve() {
        let activeTask = this.ProjectTaskService.activeTask;

        this.ProjectTaskService.approve(activeTask.id)
            .then(response => this.hideControls = true);
    }

    decline() {
        let activeTask = this.ProjectTaskService.activeTask;

        this.ProjectTaskService.decline(activeTask.id)
            .then(response => this.hideControls = true);
    }

    onUploaded(item, response) {
        item.file.is_image = response.is_image;
        item.file.extension = response.extension;
        item.file.path = response.file;

        this.files.push({file: response.file, 'original_name': item.file.name});
        this.enableForm();
    }

    removeFile(item) {
        this.files.forEach((value, key) =>{
            if (item.file.path == value.file) {
                this.files.splice(key, 1);
                return;
            }
        });

        this.FileUploader.removeFromQueue(item);
        $('#add-file')[0].value = null;
    }

    imageFilter(item) {
        return item.is_image;
    }

    disableForm() {
        this.formDisabled = true;
    }

    enableForm() {
        this.formDisabled = false;
    }
}

export default ProjectChatController;