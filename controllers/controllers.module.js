import ProjectListController from './project/list.controller';
import ProjectTaskController from './project/task/task.controller';
import ProjectDesignerController from './project/designer/designer.controller';
import ProjectCreateController from './project/create.controller';
import ProjectDiaryController from './project/diary/diary.controller';
import ProjectChatController from './project/chat/chat.controller';
import ProjectPendingController from './project/pending.controller';
import ProjectPastController from './project/past.controller';

angular.module('Controllers', [])

.controller('ProjectListController', ProjectListController)
.controller('ProjectTaskController', ProjectTaskController)
.controller('ProjectDesignerController', ProjectDesignerController)
.controller('ProjectCreateController', ProjectCreateController)
.controller('ProjectDiaryController', ProjectDiaryController)
.controller('ProjectChatController', ProjectChatController)
.controller('ProjectPendingController', ProjectPendingController)
.controller('ProjectPastController', ProjectPastController);