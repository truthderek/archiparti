require('./components/components.module.js');
require('./directives/directives.module.js');
require('./services/services.module.js');
require('./controllers/controllers.module.js');

angular.module('app', [
    'Components',
    'Directives',
    'Services',
    'Controllers',
    'Vendor'
]);

require('./routes/routes.js');
require('./interceptors/interceptors.js');

require('./pm.templates.js');
require('./pm.run.js');