require('./pm.route.js');

angular.module('app').config(
    ['$urlRouterProvider',
        function config($urlRouterProvider) {
            $urlRouterProvider.otherwise('/project');
        }
    ]
);