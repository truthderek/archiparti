angular.module('app').config(
    ['$stateProvider',
        function config($stateProvider) {
            $stateProvider
                .state('pm', {
                    url: '',
                    abstract: true,
                    views: {
                        'navbar@': {
                            template: '<header class="b-header"></header>'
                        },
                        'help@': {
                            template: '<help></help>'
                        }
                    }
                });
        }
    ]
);

require('./project/project.route.js');