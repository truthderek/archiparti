angular.module('app').config(
    ['$stateProvider',
        function config($stateProvider) {
            $stateProvider
                .state('pm.project', {
                    url: '/project',
                    views: {
                        'left-block@': {
                            controller: 'ProjectListController',
                            controllerAs: 'projectListCtrl',
                            templateUrl: 'project-list.template.html'
                        },
                        'content@': {
                            templateUrl: 'no-content.template.html'
                        }
                    }
                })

                    .state('pm.project.create', {
                        url: '/create',
                        views: {
                            'metacontent@': {
                                controller: 'ProjectCreateController',
                                controllerAs: 'createCtrl',
                                templateUrl: 'create.template.html'
                            }
                        }
                    })

                    .state('pm.project.pending', {
                        url: '/new',
                        views: {
                            'content@': {
                                controller: 'ProjectPendingController',
                                controllerAs: 'projectPendingCtrl',
                                templateUrl: 'pending.template.html'
                            }
                        }
                    })

                    .state('pm.project.past', {
                        url: '/past',
                        views: {
                            'content@': {
                                controller: 'ProjectPastController',
                                controllerAs: 'projectPastCtrl',
                                templateUrl: 'past.template.html'
                            }
                        }
                    })

                    .state('pm.project.past.show', {
                        url: '/:projectId',
                        views: {
                            'right-block@': {
                                controller: 'ProjectDiaryController',
                                controllerAs: 'projectDiaryCtrl',
                                templateUrl: 'diary.template.html'
                            },
                            'event@pm.project.past.show': {
                                templateProvider: ['ProjectEventService', function(ProjectEventService) {
                                    return ProjectEventService.template()
                                        .then(response => {
                                            return response.data.template;
                                        });
                                }]
                            }
                        }
                    })

                    .state('pm.project.show', {
                        url: '/:projectId',
                        views: {
                            'right-block@': {
                                controller: 'ProjectDiaryController',
                                controllerAs: 'projectDiaryCtrl',
                                templateUrl: 'diary.template.html'
                            },
                            'event@pm.project.show': {
                                templateProvider: ['ProjectEventService', function(ProjectEventService) {
                                    return ProjectEventService.template()
                                        .then(response => {
                                            return response.data.template;
                                        });
                                }]
                            },
                            'designer-list': {
                                controller: 'ProjectDesignerController',
                                controllerAs: 'projectDesignerCtrl',
                                templateUrl: 'designer.template.html'
                            }
                        }
                    })
                        .state('pm.project.show.designer', {
                            url: '/designer/:designerId',
                            views: {
                                'content@': {
                                    controller: 'ProjectTaskController',
                                    controllerAs: 'projectTaskCtrl',
                                    templateUrl: 'task-list.template.html'
                                },
                            }
                        })
                            .state('pm.project.show.designer.chat', {
                                url: '/chat/:taskId',
                                views: {
                                    'right-block@': {
                                        controller: 'ProjectChatController',
                                        controllerAs: 'projectChatCtrl',
                                        templateUrl: 'chat.template.html'
                                    }
                                }
                            })
        }
    ]
);