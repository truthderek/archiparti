class ConfigService {
    static $inject = [
        '$http'
    ];

    constructor($http) {
        this.http = $http;

        this.consts = {
            error: 1,
            comission: 2,
            premoderation: 3,
            ttlminutes: 4,
            ownernotifyminutes: 5
        };
    }

    list() {
        return this.http.get('/api/settings');
    }
}

export default ConfigService;