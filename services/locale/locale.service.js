import { Locale } from './locale';

class LocaleService {
    static $inject = [
        '$http',
        'LocaleLinkService'
    ];

    constructor($http, linksService) {
        this.http = $http;
        this.linksService = linksService;
        this.init();
    }

    init() {
        this.fakeLocale();
        this.fakeFallback();
        this.loadLocale();
        this.loadFallback();
    }

    get(key, bindings) {
        return this.localization.has(key) ?
            this.localization.get(key, bindings) :
            this.fallback.get(key, bindings);
    }

    change(locale) {
        return this.http.get('/api/locale/change', {params: {locale: locale}})
            .then(() => this.linksService.loadLinks())
        .then(() => this.loadLocale());
    }

    fakeLocale() {
        this.localization = new Locale({});
    }

    fakeFallback() {
        this.fallback = new Locale({});
    }

    loadLocale() {
        this.http.get('/api/locale')
            .then(response => this.localization = new Locale(response.data.localization));
    }

    loadFallback() {
        this.http.get('/api/locale/fallback')
            .then(response => this.fallback = new Locale(response.data.fallback));
    }
}

export default LocaleService;
