class LocaleLinkService {
    static $inject = [
        '$http'
    ];

    constructor($http) {
        this.http = $http;
        this.fakeLinks();
        this.loadLinks();
    }

    fakeLinks() {
        this.links = {};
    }

    loadLinks() {
        return this.http.get('/api/locale/link')
            .then(response => this.links = response.data.links);
    }
}

export default LocaleLinkService;
