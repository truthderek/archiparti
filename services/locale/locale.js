import { isProdMode } from 'app/env.js';

export class Locale {
    constructor(localeData) {
        this.localeData = localeData;
    }

    has(key) {
        return this.localeData[key] != undefined;
    }

    get(key, bindings = {}) {
        let phrase = this.ensureString(key);

        return this.applyBindings(phrase, bindings);
    }

    ensureString(key) {
        return this.localeData[key] || (isProdMode() ? '' : key);
    }

    applyBindings(phrase, bindings) {
        Object.keys(bindings).forEach(key => phrase = phrase.replace(`:${key}`, bindings[key]));

        return phrase;
    }
}
