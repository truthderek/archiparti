class SocketService {
    static $inject = ['$rootScope'];

    constructor($rootScope) {
        this.$rootScope = $rootScope;
        this.streams = {};
        this.server = 'ws://' + location.hostname + ':8080/';
    }

    open(uri, hooks = {}) {
        if (this.hasStream(uri)) {
            return this.streams[uri];
        }

        let stream = new WebSocket(this.server + uri);

        for (let hook in hooks) {
            stream[hook] = this.wrapCallback(hooks[hook]);
        }

        return this.streams[uri] = stream;
    }

    close(uri) {
        if (!this.hasStream(uri)) {
            return false;
        }

        for (let stream in this.streams) {
            if (stream.indexOf(uri) != -1) {
                this.streams[stream].close();

                delete this.streams[stream];
            }
        }
    }

    send(uri, message) {
        if (!this.hasStream(uri)) {
            return false;
        }

        this.streams[uri].send(JSON.stringify(message));
    }

    hasStream(uri) {
        let result = false;

        for (let stream in this.streams) {
            result |= (stream.indexOf(uri) != -1);
        }

        return result;
    }

    wrapCallback(callback) {
        return (socketMessage) => {
            this.$rootScope.$apply(
                callback.bind(undefined, socketMessage)
            );
        };
    }
}

export default SocketService;