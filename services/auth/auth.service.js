class AuthService {
    static $inject = [
        '$q'
    ];

    constructor($q) {
        this.q = $q;
    }

    get() {
        return this.q.resolve({
            data: {
                user: window.user
            }
        });
    }

    logout() {
        location = '/logout';
    }
}

export default AuthService;