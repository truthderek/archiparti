class ChatService {
    static $inject = ['$http'];

    constructor($http, $q) {
        this.currentFiles = {};
        this.gallery = [];
        this.http = $http;
    }

    lastMessages(taskId, page) {
        return this.http.get('/api/chat/' + taskId, {params: {page: page}});
    }

    send(message) {
        return this.http.post('/api/chat/send', {message: message});
    }

    uploadUrl(taskId) {
        return '/api/chat/' + taskId + '/upload';
    }

    files(taskId) {
        this.currentFiles = {};

        this.http.get('/api/chat/' + taskId + '/files')
            .then(response => {
                this.currentFiles = response.data.files;
                this.galleryFilter(this.currentFiles);
            });
    }

    galleryFilter(files) {
        this.gallery = files.filter(file => file.is_image).map(file => file.file);
    }
}

export default ChatService;