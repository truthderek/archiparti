import AuthService from './auth/auth.service';
import SocketService from './socket/socket.service';
import ChatService from './chat/chat.service';
import ProjectService from './project/project.service';
import ProjectEventService from './project/event.service';
import ProjectTaskService from './project/task.service';
import ProjectDesignerService from './project/designer.service';
import ProjectEventFeedService from './project/feed.service';
import ProjectInviteService from './project/invite.service.js';
import ProjectContractService from './project/contract.service.js';
import LocaleService from './locale/locale.service.js';
import LocaleLinkService from './locale/link.service.js';
import ConfigService from './config/config.service.js';

angular.module('Services', [])

.service('AuthService', AuthService)
.service('SocketService', SocketService)
.service('ChatService', ChatService)
.service('ProjectService', ProjectService)
.service('ProjectEventService', ProjectEventService)
.service('ProjectTaskService', ProjectTaskService)
.service('ProjectDesignerService', ProjectDesignerService)
.service('ProjectEventFeedService', ProjectEventFeedService)
.service('ProjectInviteService', ProjectInviteService)
.service('ProjectContractService', ProjectContractService)
.service('LocaleService', LocaleService)
.service('LocaleLinkService', LocaleLinkService)
.service('ConfigService', ConfigService);
