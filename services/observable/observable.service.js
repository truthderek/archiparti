class ObservableService {

    static $inject = [];

    constructor($rootScope) {
        $rootScope.$on('$stateChangeSuccess', this.kill.bind(this));

        this.subscribers = [];
    }

    subscribe(subscriber) {
        this.subscribers.push(subscriber);
    }

    unsubscribe(subscriber) {
        this.subsribers = this.subscribers.filter(
            currentSubscriber => currentSubscriber != subscriber
        );
    }

    kill() {
        this.subscribers.forEach((subscriber) => {
            if (!subscriber.canKill || subscriber.canKill()) {
                this.unsubscribe(subscriber);
            }
        });
    }
}

export default ObservableService;