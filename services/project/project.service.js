import ObservableService from '../observable/observable.service';

class ProjectService extends ObservableService {
    static $inject = [
        '$http',
        '$rootScope'
    ];

    constructor($http, $rootScope) {
        super($rootScope);

        this.http = $http;
        this.project = {};
        this.statuses = {
            draft: -1, premoderation: -2, todo: 0, progress: 1, done: 2
        };
    }

    list(page) {
        return this.http.get('/api/project', {params: {page: page}});
    }

    show(projectId) {
        return this.http.get('/api/project/' + projectId);
    }

    counters() {
        return this.http.get('/api/project/counters');
    }

    pending() {
        return this.http.get('/api/project/pending');
    }

    past() {
        return this.http.get('/api/project/past');
    }

    resume(projectId) {
        return this.http.get('/api/project/' + projectId + '/resume');
    }

    setActiveProject(project) {
        this.activeProject = project;

        this.notifyActiveProject();
    }

    notifyActiveProject() {
        this.subscribers
            .filter(subscriber => subscriber.notifyActiveProject != undefined)
        .forEach(subscriber => subscriber.notifyActiveProject(this.activeProject));
    }
}

export default ProjectService;
