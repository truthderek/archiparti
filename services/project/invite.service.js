import ObservableService from '../observable/observable.service';

class ProjectInviteService extends ObservableService {
    static $inject = [
        '$http',
        '$rootScope'
    ];

    constructor($http, $rootScope) {
        super($rootScope);

        this.http = $http;
    }

    decline(inviteId) {
        return this.http.get('/project/invite/' + inviteId + '/decline');
    }
}

export default ProjectInviteService;