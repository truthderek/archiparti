class ProjectContractService {
    static $inject = [
        '$http',
        '$q'
    ];

    constructor($http, $q) {
        this.http = $http;
        this.q = $q;

        this.contracts = {};
    }

    list(projectId) {
        if (this.contracts[projectId]) {
            return this.q.resolve(this.getCachedResponse(projectId));
        } else {
            return this.getPromise(projectId);
        }
    }

    getPromise(projectId) {
        let promise = this.http.get(`/api/project/contract/${projectId}`);

        promise.then(response => this.contracts[projectId] = response.data.contracts);

        return promise;
    }

    getCachedResponse(projectId) {
        return {
            data: {
                contracts: this.contracts[projectId]
            }
        };
    }
}

export default ProjectContractService;