class ProjectDesignerService {
    static $inject = [
        'ProjectService',
        'AuthService',
        '$http',
        '$q'
    ];

    constructor(ProjectService, AuthService, $http, $q) {
        this.ProjectService = ProjectService;
        this.AuthService = AuthService;
        this.http = $http;
        this.q = $q;

        this.designers = [];
        this.loadUser();
    }

    loadUser() {
        this.AuthService.get()
            .then(response => this.user = response.data.user);
    }

    progress(projectId, designers) {
        return this.http.post('/api/project/designer/progress', {project_id: projectId, designers: designers});
    }

    list(projectId) {
        let promise = this.getListPromise(projectId);

        promise.then(response => this.designersCount = response.data.designers.length);

        return promise;
    }

    getListPromise(projectId) {
        if (this.designers[projectId]) {
            return this.q.resolve(this.getCachedResponse(projectId));
        } else {
            return this.http.get('/api/project/designer/' + projectId);
        }
    }

    getCachedResponse(projectId) {
        return {
            data: {
                designers: this.filterByOwner(projectId)
            }
        };
    }

    filterByOwner(projectId) {
        return this.designers[projectId].filter(designer => {
            return (this.isAdmin() || this.isOwner() || this.isDesigner(designer));
        });
    }

    isOwner() {
        return (this.user.id == this.ProjectService.activeProject.owner_id);
    }

    isDesigner(designer) {
        return (designer.id == this.user.id);
    }

    isAdmin() {
        return this.user.is_admin;
    }
}

export default ProjectDesignerService;
