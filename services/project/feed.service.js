import ObservableService from '../observable/observable.service';

class ProjectEventFeedService extends ObservableService {
    static $inject = [
        'SocketService',
        '$rootScope'
    ];

    constructor(SocketService, $rootScope) {
        super($rootScope);

        this.SocketService = SocketService;
        this.socketOpened = false;
    }

    openSocket() {
        this.SocketService.open('event', {
            onmessage: (message) => {
                this.notifySubscribers(message.data);
            }
        });

        this.socketOpened = true;
    }

    closeSocket() {
        this.SocketService.close('event');
        this.socketOpened = false;
    }

    notifySubscribers(message) {
        this.subscribers.forEach(function(subscriber) {
            subscriber.notify(message);
        });
    }
}

export default ProjectEventFeedService;