import ObservableService from '../observable/observable.service';

class ProjectEventService extends ObservableService {
    static $inject = [
        '$http',
        '$rootScope'
    ];

    constructor($http, $rootScope) {
        super($rootScope);

        this.http = $http;
        this.types = {
            newmessage: 1,
            taskstatus: 2,
            deadline: 3
        };
    }

    dropdown() {
        return this.http.get('/api/project/event/dropdown');
    }

    template() {
        return this.http.get('/api/project/event/template');
    }

    list(page, projectId) {
        return this.http.get('/api/project/event/' + projectId, {params: {page: page}});
    }

    readByEvents(events) {
        let promise = this.http.post('/api/project/event/read', {events: events});

        promise.then(() => this.notifySubscribers());

        return promise;
    }

    readByMessages(messages) {
        let promise = this.http.post('/api/project/event/message-read', {messages: messages});

        promise.then(() => this.notifySubscribers());

        return promise;
    }

    notifySubscribers() {
        this.subscribers.filter(subscriber => subscriber.notifyEventRead)
            .forEach(subscriber => subscriber.notifyEventRead());
    }
}

export default ProjectEventService;