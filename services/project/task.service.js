import ObservableService from '../observable/observable.service';

class ProjectTaskService extends ObservableService {
    static $inject = [
        '$http',
        '$state',
        '$rootScope'
    ];

    constructor($http, $state, $rootScope) {
        super($rootScope);

        this.http = $http;
        this.state = $state;
        this.reviewsLimit = 2;
        this.currentTasks = {};

        this.setStatuses();
    }

    setStatuses() {
        this.statuses = {
            progress: { index: 1, name: 'In progress' },
            review: { index: 2, name: 'In review' },
            done: { index: 3, name: 'Done' }
        };
    }

    getProjectId() {
        return this.state.params.projectId;
    }

    getDesignerId() {
        return this.state.params.designerId;
    }

    list(status) {
        return this.http.get(
            `/api/project/task/${this.getProjectId()}/${this.getDesignerId()}`,
            {params: {status: status}}
        );
    }

    show(taskId) {
        return this.http.get('/api/project/task/show/' + taskId);
    }

    approve(taskId) {
        this.notifyTaskStatus(taskId, this.statuses.done.index);

        return this.http.get('/api/project/task/approve/' + taskId);
    }

    decline(taskId) {
        this.notifyTaskStatus(taskId, this.statuses.progress.index);

        return this.http.get('/api/project/task/decline/' + taskId);
    }

    review(taskId) {
        this.notifyTaskStatus(taskId, this.statuses.review.index);

        return this.http.get('/api/project/task/review/' + taskId);
    }

    add(projectId, designerId, task) {
        return this.http.post('/api/project/task/' + `${projectId}/${designerId}/add`, {task: task});
    }

    delete(tasks) {
        return this.http.post('/api/project/task/delete', {tasks: tasks});
    }

    notifyTaskStatus(taskId, newStatus) {
        this.changeActiveTaskStatus(taskId, newStatus);

        this.subscribers
            .filter(subscriber => subscriber.notifyTaskStatus != undefined)
        .forEach(subscriber => subscriber.notifyTaskStatus(taskId, newStatus));
    }

    notifyTasks() {
        this.subscribers
            .filter(subscriber => subscriber.notifyTasks != undefined)
        .forEach(subscriber => subscriber.notifyTasks(this.currentTasks));
    }

    setCurrentTasks(tasks) {
        this.currentTasks = tasks;

        this.notifyTasks();
    }

    changeActiveTaskStatus(taskId, newStatus) {
        if (this.activeTask && (this.activeTask.id == taskId)) {
            this.activeTask.status = newStatus;
        }
    }
}

export default ProjectTaskService;
