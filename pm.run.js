angular.module('app')
    .run(['$rootScope', 'SocketService',
        function($rootScope, SocketService) {
            $rootScope.$on('$stateChangeStart',
                function(e, newState, newParams, oldState, oldParams) {
                    if (oldState.name && (newParams.taskId != oldParams.taskId)) {
                        SocketService.close('chat');
                    }
                }
            );
        }
    ])

    .run(['$rootScope', 'ProjectEventFeedService',
        function($rootScope, ProjectEventFeedService) {
            $rootScope.$on('$stateChangeStart', function() {
                if (!ProjectEventFeedService.socketOpened) {
                    ProjectEventFeedService.openSocket();
                }
            });
        }
    ]);
