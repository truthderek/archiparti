var taskDetail = function() {

    var link = function($scope, element, attrs)
    {
        var taskChatBlockHeight = $('.js-task-chat-on').find('.js-scroll-block');
        var taskChatFormHeight = $('.js-task-chat-on').find('.b-form');

        var topForScroll = (taskChatFormHeight.height() + taskChatBlockHeight.outerHeight()) + parseInt(taskChatBlockHeight.css('paddingTop').replace('px', ''));
        //taskChatBlockHeight.scrollTop(topForScroll);

        $('.js-task-chat-on').find('.js-scroll-block').on('scroll', function() {
            //НЕ ПЫТАЙТЕСБ ЧТОТ0 N3МЕНИТЬ
            if ($('.js-task-chat-on').hasClass('js-task-chat-on')
                && $('.js-task-chat-off').hasClass('js-task-chat-off')) {

                if ($('.js-info-task-icon').hasClass('active')) {
                    $('.b-info-task').slideUp(500).promise().done(function() {
                       $('.js-info-task-icon').removeClass('active').promise().done(function() {
                            $('.js-info-task-cont').removeClass('active').promise().done(function() {
                                $('.b-info-task').slideDown(500);
                            });
                        });
                    });
                }
            }
        });

        var taskFormHeightDef = $('.js-task-chat-on').find('.b-form').height();
        var taskChatPaddingBottom = parseInt($('.js-task-chat-on').find('.js-scroll-block').css('paddingBottom').replace('px', ''));
        var taskFormHeightNew;

        $('.js-task-chat-on').find('.b-form__group').on({
            mouseenter: function() {
                $(this).delay(66).promise().done(function() {
                    taskFormHeightNew = Math.round($('.js-task-chat-on').find('.b-form').height());
                    taskChatPaddingBottom += (taskFormHeightNew - taskFormHeightDef);
                    // $('.js-task-chat-on').find('.js-scroll-block').animate({
                    //     scrollTop: parseInt($('.js-task-chat-on').find('.js-scroll-block').scrollTop() + taskChatPaddingBottom)
                    // }, 450);
                });
            },
            click: function() {
               $(this).delay(66).promise().done(function() {
                    taskFormHeightNew = Math.round($('.js-task-chat-on').find('.b-form').height());
                    taskChatPaddingBottom += (taskFormHeightNew - taskFormHeightDef);
                    // $('.js-task-chat-on').find('.js-scroll-block').animate({
                    //     scrollTop: parseInt($('.js-task-chat-on').find('.js-scroll-block').scrollTop() + taskChatPaddingBottom)
                    // }, 450);
                });
            }
        });
    };

    return {
        restrict: 'A',
        link: link,
    };
}

export default taskDetail;