var groupSelect = ['$compile', function($compile) {
    let link = function($scope, element, attrs){
        var data;

        //select group
        $(".js-select-group").select2({
            theme: "default",
            tags: true,
            // minimumResultsForSearch: Infinity,
            width: "100%",
            minimumResultsForSearch: -1,
            placeholder: function(){
                $(this).data('placeholder');
            },
            templateResult: formatCircle,
            templateSelection: formatCircle        
        }); 

        //добавление блока Add a new group в конце поля селекта
        function finedSelectOpen(what, where, openOrClose) {
            if (openOrClose) {
                if ($('.select2-results__options').length > 0) {
                    setTimeout(function() {
                        let template = $compile(what)($scope);                   
                        where.append(template);
                        // hiddenInputAddGroup();
                    }, 30);
                } else {
                    finedSelectOpen(what, where);
                } 
            } else {
                let template = $compile(what)($scope);
                where.append(template);
                hiddenInputAddGroup();
            }
            
        }

        var $eventSelectOpen = $(".js-select-group");
        $eventSelectOpen.on("select2:open", function (e) {
         var permutationBlock = $('.js-add-group'),
            permutationHereBlock = $('.select2-results__options');
            finedSelectOpen(permutationBlock, permutationHereBlock, true); 
         });


        var $eventSelectClose = $(".js-select-group");
        $eventSelectClose.on("select2:close", function (e) {
         var permutationBlock = $('.js-add-group'),
            permutationDefaultBlock = $('.js-permutation-default');
            finedSelectOpen(permutationBlock, permutationDefaultBlock, false); 
         });

        //добавление input Add new group в конце поля селекта вместо блока со ссылкой

        var addGroupContainer = $('.js-add-group');  
        function showInputAddGroup(event) {        
            var target = event.target;
            var inputAddGroup = $('.js-add-input-group');  
             
            if(!target.classList.contains('js-link-add-group')) return;
            addGroupContainer.removeClass('js-hidden-add-task');
           /* target.style.display = 'none';
            inputAddGroup.attr('style', 'display: flex');  */  
        }

        $(document).on('click', '.js-add-group', showInputAddGroup);

        //скрытие input Add new group

        function hiddenInputAddGroup() { 
            addGroupContainer.addClass('js-hidden-add-task');
            console.log('event.type');
        }

        //добавление цветных кругов к пунктам селекта
        function formatCircle (circle) {

          if (!circle.id) { return circle.text;}

          var $circle = $(
            //class="select-circle" placed in b-select.tablet
            '<span class="select-item select-item--group"><span class="select-circle" style="background-color:' + circle.element.dataset.color + ';"></span> ' + circle.text + '</span>'
        );      
        return $circle; 
        };

        $(document).on("click", ".js-button-add-group", function(){
          var newStateVal = $("#new-group").val();
          // Set the value, creating a new option if necessary
          if ($(".js-select-group").find("option[value='" + newStateVal + "']").length) {
            $(".js-select-group").val(newStateVal).trigger("change");
          } else { 
            // Create the DOM option that is pre-selected by default
            var newState = new Option(newStateVal, newStateVal, true, true);
            // Append it to the select
            //$(".js-select-group").append(newState).trigger('change');
          } 
        }); 

        //селект assign


        $(".js-select-project").select2({
            theme: "default",
            // minimumResultsForSearch: Infinity,
            width: "100%",
            minimumResultsForSearch: -1,
            placeholder: function(){
                $(this).data('placeholder');
            },   
            templateResult: formatState,
            templateSelection: formatState
        });   

        //добавление изображений к пунктам селекта
        function formatState (state) {
          if (!state.id) { return state.text; }
          var $state = $(
            '<span class="select-item select-item--assigned"><span class="img-select-container"><img src="api/user/' + state.element.value + '/avatar/36x36?' + state.element.dataset.avatar + '.png" class="img-select" /></span> ' + state.text + '</span>'
        );
        return $state;
        };
    };

    return {
        restrict: 'A',
        link: link
    };
}]

export default groupSelect;