var urlWrapper = function() {
    let link = function($scope, element, attrs) {
        let replacement = $scope.replacement ? $scope.replacement : 'link';

        setTimeout(function() {
            element.html(wrapUrl(element.text(), replacement));
        });
    };

    function wrapUrl(text, replacement) {
        let regEx = /https?:\/\/[^\s]+/g;

        return text.replace(regEx, function(url) {
            return `<a href='${url}' target='_blank'>${replacement}</a>`;
        });
    }

    return {
        restrict: 'A',
        scope: {
            replacement: '@'
        },
        link: link
    };
};

export default urlWrapper;