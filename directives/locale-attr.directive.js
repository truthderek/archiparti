const localeAttr = ['LocaleService', function (locales) {
    const applyAttr = function ($scope, $element, $attrs) {
        $element.attr($scope.localeAttr, computeLocalization($scope));
        $attrs[$scope.localeAttr] = computeLocalization($scope);
    };

    const computeLocalization = function ($scope) {
        return parsePrefix($scope.localePrefix) + locales.get($scope.localeVariable, $scope.localeBindings);
    };

    const parsePrefix = function (prefix) {
        return prefix ? prefix + ' ' : '';
    };

    const link = function ($scope, $element, $attrs) {
        $scope.$watch(
            () => computeLocalization($scope),
            () => applyAttr(...arguments)
        );
    };

    return {
        restrict: 'A',
        link: link,
        scope: {
            localeAttr: '@',
            localeVariable: '@',
            localeBindings: '<',
            localePrefix: '@'
        }
    };
}];

export default localeAttr;
