var rightAside = ['$state', function($state) {
    var link = function($scope, $element) {
        $scope.$watch(
            function () {
                return $state.current.name;
            },
            function (name) {
                if (!['pm.project', 'pm.project.pending', 'pm.project.past'].includes(name)) {
                    $element.css('display', 'block');
                } else {
                    $element.hide();
                }
            }
        );
    };

    return {
        restrict: 'A',
        link: link
    };
}];

export default rightAside;