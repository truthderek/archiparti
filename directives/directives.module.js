import toggleOnHover from './toggle-on-hover.directive';
import projectPercent from './project-percent.directive';
import rightAside from './right-aside.directive';
import infiniteScroll from './infinite-scroll.directive';
import taskDetail from './task-detail.directive';
import projectList from './project-list.directive';
import filesList from './files-list.directive';
import tooltipPosition from './tooltip.directive';
import tooltipRight from './tooltip-right.directive';
import slickSlider from './slick-slider.directive';
import taskChat from './task-chat.directive';
import taskIcon from './task-icon.directive';
import jsMenu from './js-menu.directive';
import loadingCircle from './loading-circle/loading-circle.directive';
import urlWrapper from './url-wrapper.directive';
import locale from './locale.directive';
import localeAttr from './locale-attr.directive';
import addTaskValidation from './add-task-validation.directive';
import deleteTaskValidation from './delete-task-validation.directive';
import groupSelect from './group-select.directive';

angular.module('Directives', [])

.directive('toggleOnHover', toggleOnHover)
.directive('projectPercent', projectPercent)
.directive('rightAside', rightAside)
.directive('infiniteScroll', infiniteScroll)
.directive('taskDetail', taskDetail)
.directive('projectList', projectList)
.directive('filesList', filesList)
.directive('tooltipPosition', tooltipPosition)
.directive('tooltipRight', tooltipRight)
.directive('slickSlider', slickSlider)
.directive('taskChat', taskChat)
.directive('taskIcon', taskIcon)
.directive('jsMenu', jsMenu)
.directive('loadingCircle', loadingCircle)
.directive('urlWrapper', urlWrapper)
.directive('locale', locale)
.directive('localeAttr', localeAttr)
.directive('addTaskValidation', addTaskValidation)
.directive('groupSelect', groupSelect)
.directive('deleteTaskValidation', deleteTaskValidation);
