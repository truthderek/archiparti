var projectPercent = function() {
    var link = function($scope, element, attrs) {
        var watchPercent = function() {
            $scope.$watch(
                function() {
                    return attrs.percent;
                },
                function(percent) {
                    if (percent) {
                        updateClass(percent);
                    }
                }
            );
        };

        var updateClass = function(percent) {
            var percentClass;

            if (percent >= 10 && percent < 50) percentClass = 'ten-percent'
            else if (percent >= 50 && percent < 90) percentClass = 'fifty-percent'
            else if (percent >= 90 && percent < 100) percentClass = 'ninety-percent'
            else if (percent == 100) percentClass = 'done';

            if (percentClass) {
                element.addClass('b-project-percent--' + percentClass);
            }
        };

        watchPercent();
    };

    return {
        restrict: 'A',
        link: link,
    };
}

export default projectPercent;