var projectList = function() {
    var link = function($scope, $element) {
        $element.on('scroll', function() {
            var count = 0;
            var elements = $element.find('.b-project-list__item');
            var elemHeight = elements.first().height();
            var lastElem = Math.floor($element.scrollTop() / elemHeight);

            elements.removeClass('b-project-list__item--visible b-project-list__item--transparent');

            elements.each(function(i, val) {
                if (i >= lastElem) {
                    if (count <= 3) {
                        $(this).addClass('b-project-list__item--visible');
                        count++;
                    } else {
                        $(this).addClass('b-project-list__item--transparent');
                        count++;
                    }
                }
            });
        });
    };

    return {
        restrict: 'A',
        link: link
    };
};

export default projectList;