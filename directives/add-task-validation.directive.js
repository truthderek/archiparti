var addTaskValidation = function() {
    var link = function($scope, element, attrs) {
        var allMouth = [31,28,31,30,31,30,31,31,30,31,30,31];
            var dataValid = null;
            $('.js-data-mask').mask("99.99.9999", {
                completed: function() {
                    var now = new Date();
                    var currentDay = now.getDate();
                    var currentMonth = now.getUTCMonth();
                    var currentYear = now.getUTCFullYear();
                    var maxYear = currentYear + 3;
                    var arValueDate = this.val().split('.');
                    
                    if (currentYear == parseInt(arValueDate[2])) {

                        
                        if (currentMonth == (parseInt(arValueDate[1]) - 1)) {

                            
                            if (arValueDate[0] != currentDay && arValueDate[0] > currentDay && arValueDate[0] != 0
                                && arValueDate[0] <= allMouth[(parseInt(arValueDate[1]) - 1)]) {
                                dataValid = true;
                            } else {
                                dataValid = false;
                            }

                        
                        } else if (currentMonth < (parseInt(arValueDate[1]) - 1)) {

                            
                            if (arValueDate[0] <= allMouth[(parseInt(arValueDate[1]) - 1)] && arValueDate[0] != 0) {
                                dataValid = true;
                            } else {
                                dataValid = false;
                            }
                        } else {
                            dataValid = false;
                        }
                    } else if (currentYear < arValueDate[2] && maxYear >= arValueDate[2]) {

                        
                        if (parseInt(arValueDate[1]) <= allMouth.length) {

                            
                            if (arValueDate[0] <= allMouth[(parseInt(arValueDate[1]) - 1)] && arValueDate[0] != 0) {
                                dataValid = true;
                            } else {
                                dataValid = false;
                            }
                        }
                    } else {
                        dataValid = false;
                    }
                }
            });

            function validationAddPopup(count, maxCount) {
                if (count == maxCount) {
                    return true;
                } else {
                    return false;
                }
            }

            function dataKeyValid(data) {
                if (data.length > 0) {
                    if ((data.indexOf("_") + 1) == 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }

            function getCountAllElement(count, elements, dataValidation) {
                count = 0;
                elements.each(function(i, val) {
                    if ($(this).hasClass("js-data-mask")) {
                        var boolenKey = dataKeyValid($(this).val());
                        if (dataValidation && boolenKey) {
                            $(this).css("border", "");
                            count++;
                        }
                    } else if ($(this).attr("type") == "text") {
                        if ($(this).val().length > 0) {
                            $(this).css("border", "");
                            count++;
                        }
                    } else {
                        if ($(this).val().length > 0) {
                            $(this).css("border", "");
                            count++;
                        }
                    }
                });
                return count;
            }

            function findEmptyElement(elements, dataValidation, callback) {
                callback = callback || false;
                elements.each(function(i, val) {
                    if ($(this).hasClass("js-data-mask")) {
                        var boolenKey = dataKeyValid($(this).val());
                        if (dataValidation && boolenKey) {
                            $(this).css("border", "");
                            $(this).removeClass('error');
                        } else {
                            $(this).css("border", "solid 1px #e60002");
                            $(this).addClass('error');
                        }
                    } else if ($(this).attr("type") == "text") {
                        if ($(this).val().length > 0) {
                            $(this).css("border", "");
                            $(this).removeClass('error');
                        } else {
                            $(this).css("border", "solid 1px #e60002");
                            $(this).addClass('error');
                        }
                    } else {
                        if ($(this).val().length > 0) {
                            $(this).parent().css("border", "");
                            $(this).removeClass('error');
                        } else {
                            $(this).parent().css("border", "solid 1px #e60002");
                            $(this).addClass('error');
                        }
                    }
                });
                if (callback != false) callback();
            }

            var clearKeydown;
            var elementsInAddTask = $(".js-valid-add-popup").find("input.js-required-add-task, select.js-required-add-task");
            var countValidMax = elementsInAddTask.length;
            var countValid = 0;
            validationAddPopup(countValid, countValidMax);

            elementsInAddTask.each(function(i, val) {
                if ($(this).hasClass("js-data-mask")) {
                    $(this).on('keydown', function() {
                        clearTimeout(clearKeydown);
                        clearKeydown = setTimeout(function() {
                            findEmptyElement(elementsInAddTask, dataValid);
                            countValid = getCountAllElement(countValid, elementsInAddTask, dataValid);
                            validationAddPopup(countValid, countValidMax);
                        }, 125);
                    });
                } else if ($(this).attr("type") == "text") {
                    $(this).on('keydown', function() {
                        clearTimeout(clearKeydown);
                        clearKeydown = setTimeout(function() {
                            findEmptyElement(elementsInAddTask, dataValid);
                            countValid = getCountAllElement(countValid, elementsInAddTask, dataValid);
                            validationAddPopup(countValid, countValidMax);
                        }, 125);
                    });
                } else {
                    $(this).on("change", function() {
                        findEmptyElement(elementsInAddTask, dataValid);
                        countValid = getCountAllElement(countValid, elementsInAddTask, dataValid);
                        validationAddPopup(countValid, countValidMax);
                        redBorder(false);
                        redBorder(true);
                    });
                }
            });


            $('.js-valid-add-popup').find(".js-validation-form").submit(function() {
                findEmptyElement(elementsInAddTask, dataValid);
                if (!validationAddPopup(countValid, countValidMax))
                    return false;
            });

            

            var $eventSelectOpenProject = $('.js-select-project');
            var $eventSelectOpenGroup = $('.js-select-group');
            $eventSelectOpenProject.on("select2:open", function() {
                redBorder(true);
            });
            $eventSelectOpenGroup.on("select2:open", function() {
                redBorder(false);
            });

            function redBorder(project) {
                if ($('.select2-results__options').length > 0) {
                    setTimeout(function() {
                        if (project) {
                            if($('#assigned').hasClass('error')) {
                                $('#select2-assigned-results').css({
                                    "border": "solid 1px #e60002",
                                    "border-top": "none"
                                });
                            } else if(!$('#assigned').hasClass('error')) {
                                $('#select2-assigned-results').css("border", "");
                            }
                        } else {
                            if($('#group').hasClass('error')) {
                                $('#select2-group-results').css({
                                    "border": "solid 1px #e60002",
                                    "border-top": "none"
                                });
                                $('.js-add-group').css({
                                    "border": "solid 1px #e60002",
                                    "border-top": "none"
                                });
                            } else if(!$('#group').hasClass('error')) {
                                $('#select2-group-results').css("border", "");
                                $('.js-add-group').css("border", "");
                            }
                        }

                    }, 30);
                }
            }
    };

    return {
        restrict: 'A',
        link: link,
    }
};

export default addTaskValidation;