import template from './loading-circle.template.html';

let loadingCircle = ['$timeout', function ($timeout) {
    let link = function (scope, elem, attrs) {
        let img = elem.find('img');
        let i = 20;

        let rotate = function () {
            img.css('transform', "rotate(" + i + "deg)");
            i += 20;

            $timeout(rotate, 100);
        };

        $timeout(rotate);
    };

    return {
        restrict: 'E',
        template: template,
        link: link
    };
}];

export default loadingCircle;