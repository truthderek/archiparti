var deleteTaskValidation = function() {
    var link = function($scope, element, attrs) {
        $(document).ready(function() {
            function validationDeletePopup() {
                var checkElements = $(".js-valid-delete-popup").find("input:checked");
                if (checkElements.length >= 1 ) {
                    $(".js-link-delete-task-popup").css("background", "");
                    return true;
                } else {
                    $(".js-link-delete-task-popup").css("background", "#9b9b9b");
                    return false;
                }
            }
            validationDeletePopup();

            $('.b-form-delete-task').on('click', function(e) {
                let group = $(e.target).closest('.js-delete-tasks');
                let task = $(e.target).closest('.js-delete-task');

                if (group[0]) {
                    var nextElement = group.next();
                    if (group.find('input').prop("checked")) {
                        $(group).find('input').prop("checked", false);
                        nextElement.find("input").each(function(i, val) {
                            $(this).prop("checked", false);
                        });
                    } else {
                        $(group).find('input').prop("checked", true);
                        nextElement.find("input").each(function(i, val) {
                            $(this).prop("checked", true);
                        });
                    }
                    validationDeletePopup();
                }


                if (task[0]) {
                    var parentElement = task.parent();
                    var elementDeleteTasks = parentElement.prev();
                    var countChecked = 0;
                    if (task.find('input').prop("checked"))
                        task.find('input').prop("checked", false);
                    else
                        task.find('input').prop("checked", true);

                    parentElement.find("input").each(function(i, val) {
                        if ($(this).prop("checked")) {countChecked++;}
                    });

                    if (countChecked == parentElement.find("input").length) {
                        elementDeleteTasks.find("input").prop("checked", true);
                    } else {
                        elementDeleteTasks.find("input").prop("checked", false);
                    }
                    validationDeletePopup();
                }
            });

            $(".js-link-delete-task-popup").on("click", function() {
                if (!validationDeletePopup())
                    return false;
            });
        });
    }

    return {
        restrict: 'A',
        link: link,
    }
};

export default deleteTaskValidation;