var infiniteScroll = function() {
    var link = function($scope, $element, $attrs) {
        var previous, current, max, enabled, busy;
        var inversed = $scope.infiniteScrollInversed;

        previous = current = max = 0;

        $attrs.onScroll = function($psScope, params) {
            enabled = !$scope.infiniteScrollDisabled;
            current = params.scrollTop;
            max = params.scrollHeight;

            var scrolledDown = (current > previous);

            if (!scrolledDown && inversed) {
                scrollTop();
            }

            if (scrolledDown && !inversed) {
                scrollDown();
            }

            previous = current;
        };

        var scrollDown = function() {
            if ((max - current < max / 3) && enabled && !busy) {
                $scope.infiniteScroll();
                scrollHeightWatcher();

                busy = true;
            }
        };

        var scrollTop = function() {
            if ((current < max / 3) && enabled && !busy) {
                $scope.infiniteScroll();
                scrollHeightWatcher();

                busy = true;
            }
        };

        var scrollHeightWatcher = function() {
            var unwatch = $scope.$watch(
                function() { return $element.prop('scrollHeight'); },
                function(updated, old) {
                    if (updated > old) {
                        unwatch();
                        busy = false;

                        if (inversed) {
                            returnScrollPosition(updated, old);
                        }
                    }
                }
            );
        };

        var returnScrollPosition = function(updated, old) {
            $element.scrollTop(updated - old + current);
            $element.perfectScrollbar('update');
        };

        if (inversed) {
            scrollHeightWatcher();
        }
    };

    return {
        restrict: 'A',
        link: link,
        scope: {
            infiniteScrollDisabled: '<',
            infiniteScrollInversed: '<',
            infiniteScroll: '&'
        }
    };
};

export default infiniteScroll;