const locale = ['LocaleService', function (locales) {
    const link = function ($scope, $element) {
        $scope.$watch(
            () => locales.get($scope.locale, $scope.localeBindings),
            () => $element.text(locales.get($scope.locale, $scope.localeBindings))
        );
    };

    return {
        restrict: 'A',
        link: link,
        scope: {
            locale: '@',
            localeBindings: '<'
        }
    };
}];

export default locale;
