var toggleOnHover = ['$timeout', '$state', 'ProjectDesignerService',
    function($timeout, $state, ProjectDesignerService) {

        var link = function($scope, $element) {
            var timer = false;

            var countWatcher = function() {
                var unwatch = $scope.$watch(
                    function() { return ProjectDesignerService.designersCount; },
                    function(designersCount) {
                        if ($scope.list != 'studio') {
                            $scope.list = 'min';
                        }

                        if (designersCount > 1 && $scope.list != 'studio') {
                            enterStudio();
                            unwatch();
                        }
                    }
                );
            };

            let enterStudio = function () {
                $scope.list = 'min';

                $timeout(function () {
                    $scope.list = 'studio';
                }, 500);
            };

            $scope.$watch(
                function () { return $state.current.name; },
                function (name) {
                    if (name !== 'pm.project') {
                        registerListeners();
                        countWatcher();
                    } else {
                        $element.off();
                        $scope.list = 'max';
                    }
                }
            );

            let registerListeners = function () {
                registerMouseLeave();
                registerMouseOver();
            };

            let registerMouseLeave = function () {
                $element.on('mouseleave', function () {
                    timer = false;

                    $timeout(function () {
                        countWatcher();
                        if (ProjectDesignerService.designersCount <= 1) {
                            $scope.list = 'min';
                        }
                    }, 300);
                });
            };

            let registerMouseOver = function () {
                $element.on('mouseenter', function () {
                    timer = true;
                    $timeout(function () {
                        if (timer) {
                            if (ProjectDesignerService.designersCount > 1) {
                                $scope.list = 'min';
                            }

                            $timeout(function () {
                                $scope.list = 'max';
                            }, 500);

                            timer = false;
                        }
                    }, 750);
                });
            };
        };

        return {
            restrict: 'A',
            link: link,
            scope: {
                list: '=list',
            }
        };
    }
];

export default toggleOnHover;