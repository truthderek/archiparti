var filesList = function() {
    var link = function($scope, element, attrs) {
        $('.js-files-block').on('click', function() {
            $('.js-look-file').addClass('active').promise().done(function() {
                $('.js-look-file').slideUp(0).promise().done(function() {
                    $('.js-look-file').delay(300).slideDown(625);
                });
            });
        });

        $('.js-close-files-block').on('click', function() {
            $('.js-look-file').slideUp(625).promise().done(function() {
                $('.js-look-file').removeClass('active');
            });
        });
    };

    return {
        restrict: 'A',
        link: link,
    }
};

export default filesList;