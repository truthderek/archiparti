var slickSlider = function() {
    var link = function($scope, element, attrs) {
        $('.js-slick-slider').slick({
            dots: false,
            arrows: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 0,
            prevArrow: '<a href="javascript:void();" class="b-modal-gallery__carousel-control carousel-control left"><i class="icon-back"></i></a>',
            nextArrow: '<a href="javascript:void();" class="b-modal-gallery__carousel-control carousel-control right"><i class="icon-send"></i></a>'
        });

        element.on('show.bs.modal', function (e) {
            $('.js-slick-slider').slick('refresh');
        });

        element.on('show.bs.modal', animate.bind(this, element));
        element.on('hidden.bs.modal', hide.bind(this, element));
    };

    $(document).ready(function() {
        $('.modal-vertical-align').on('show.bs.modal', function(e) {
            centerModals($(this));
        });
    });


    function centerModals($element) {
        var $modals;
        if ($element.length) {
            $modals = $element;
        } else {
            $modals = $(modalVerticalCenterClass + ':visible');
        }
        $modals.each( function(i) {
            var $clone = $(this).clone().css('display', 'block').appendTo('body');
            var top = Math.round(($clone.height() - $clone.find('.b-modal').height()) / 2);
            top = top > 0 ? top : 0;
            $clone.remove();
            $(this).find('.b-modal').css("margin-top", top);
        });
    };

    var animate = function(element, event) {
        var cloneElementShadow;

        element.css({display: 'none'}).promise().done(function() {
            $('.js-clone-element').remove().promise().done(function() {
                cloneElementShadow = $('.modal-backdrop').clone().addClass('js-clone-element').css({display: 'none'});
                $('body').append(cloneElementShadow);
                $('.modal-backdrop').first().remove();
            }).promise().done(function() {
                element.css({
                    display: "block",
                    opacity: 0
                }).promise().done(function() {
                    $('.js-slick-slider').slick('refresh');
                    element.delay(150).animate({
                        opacity: 1
                    }, 500);
                    cloneElementShadow.fadeIn(500);
                    setTimeout(function() {
                        $('.js-slick-slider').slick('slickGoTo', $(event.relatedTarget).data('index'));
                    }, 500);
                });
            });
        });
    };

    var hide = function (element) {
        element.css({display: 'block'}).promise().done(function() {
            element.delay(150).fadeOut(500);
            $('.modal-backdrop').fadeOut(500);
        });
    };

    return {
        restrict: 'A',
        link: link
    };
};

export default slickSlider;