angular.module('app')
    .run(['$templateCache', function($templateCache) {
        $templateCache.put('project-list.template.html', require('./templates/project/list/list.template.html'));
        $templateCache.put('project-list-inner.template.html', require('./templates/project/list/list-inner.template.html'));
        $templateCache.put('no-content.template.html', require('./templates/project/list/no-content.template.html'));
        $templateCache.put('create.template.html', require('./templates/project/create.template.html'));
        $templateCache.put('diary.template.html', require('./templates/project/diary/diary.template.html'));
        $templateCache.put('designer.template.html', require('./templates/project/designer/designer.template.html'));

        $templateCache.put('task-list.template.html', require('./templates/project/task/list.template.html'));

        $templateCache.put('chat.template.html', require('./templates/project/chat/chat.template.html'));
        $templateCache.put('past.template.html', require('./templates/project/past/past.template.html'));

        $templateCache.put('pending.template.html', require('./templates/project/pending/pending.template.html'));
        $templateCache.put('invited-with-offer.template.html', require('./templates/project/pending/invited-with-offer.template.html'));
        $templateCache.put('invited-without-offer.template.html', require('./templates/project/pending/invited-without-offer.template.html'));
        $templateCache.put('owned.template.html', require('./templates/project/pending/owned.template.html'));
        $templateCache.put('premoderation.template.html', require('./templates/project/pending/premoderation.template.html'));
    }]);