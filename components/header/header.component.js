import HeaderComponentTemplate from './header.template.html';

class HeaderComponentController {
    static $inject = [
        '$state',
        'AuthService',
        'LocaleService',
        'LocaleLinkService',
    ];

    constructor($state, AuthService, LocaleService, LocaleLinkService) {
        this.AuthService = AuthService;
        this.$state = $state;
        this.linksService = LocaleLinkService;
        this.localeService = LocaleService;

        this.getUser();
    }

    getUser() {
        this.promise = this.AuthService.get();

        this.promise.then(response => this.user = response.data.user);
    }

    logout() {
        this.AuthService.logout();
    }

    redirect() {
        if (!this.user) {
            this.promise.then(() => this.performRedirect());
        } else {
            this.performRedirect();
        }
    }

    performRedirect() {
        if (this.user.is_designer) {
            location = '/portfolio';
        } else {
            location = '/profile';
        }
    }
}

const Header = {
    controller: HeaderComponentController,
    template: HeaderComponentTemplate
};

export default Header;