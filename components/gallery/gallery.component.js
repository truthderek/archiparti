import GalleryComponentTemplate from './gallery.template.html';

class GalleryComponentController {
    static $inject = [
        'ChatService',
        '$state',
        '$scope'
    ];

    constructor(ChatService, $state, $scope) {
        this.ChatService = ChatService;

        $scope.$watchCollection(() => $state.params, () => {
            if ($state.params.taskId) {
                this.getFiles($state.params.taskId);
            }
        });
    }

    getFiles(taskId) {
        this.ChatService.files(taskId);
    }

    imageFilter(item) {
        return item.is_image;
    }
}

const Gallery = {
    controller: GalleryComponentController,
    template: GalleryComponentTemplate
};

export default Gallery;