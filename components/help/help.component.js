import HelpComponentTemplate from './help.template.html';

class HelpComponentController {
    static $inject = [
        '$state'
    ];

    constructor($state) {
        this.$state = $state;
    }
}

const Help = {
    controller: HelpComponentController,
    template: HelpComponentTemplate
};

export default Help;