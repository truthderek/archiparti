import template from './contract-link.template.html';

class ContractLinkComponentController {
    static $inject = [
        'ProjectContractService',
        '$state'
    ];

    constructor(ProjectContractService, $state) {
        this.ProjectContractService = ProjectContractService;
        this.state = $state;
    }

    $onInit() {
        this.contracts = [];
        this.loadContracts();
    }

    loadContracts() {
        this.ProjectContractService.list(this.state.params.projectId)
            .then(response => this.contracts = response.data.contracts);
    }

    linkToContract() {
        return `/project/contract/${this.findContract().id}/pdf`;
    }

    findContract() {
        let designerId = this.designerId || this.state.params.designerId;

        return this.contracts.find(contract => contract.user_id == designerId) || {};
    }
}

const ContractLink = {
    controller: ContractLinkComponentController,
    template: template,
    bindings: {
        designerId: '<',
        classes: '@'
    }
};

export default ContractLink;
