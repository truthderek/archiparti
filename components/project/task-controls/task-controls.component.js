import template from './task-controls.template.html';

class TaskControlsComponentController {
    static $inject = [
        'ProjectTaskService',
        'ProjectService',
        'AuthService'
    ];

    constructor(ProjectTaskService, ProjectService, AuthService) {
        this.ProjectTaskService = ProjectTaskService;
        this.ProjectService = ProjectService;
        this.AuthService = AuthService;
    }

    $onInit() {
        this.taskStatuses = this.ProjectTaskService.statuses;
        this.loadUser();
    }

    loadUser() {
        this.AuthService.get().then(response => this.user = response.data.user);
    }

    reviewsLimitReached() {
        return (this.task.reviews_count >= this.ProjectTaskService.reviewsLimit);
    }

    inReview() {
        return (this.task.status == this.taskStatuses.review.index);
    }

    inProgress() {
        return (this.task.status == this.taskStatuses.progress.index);
    }

    inDone() {
        return (this.task.status == this.taskStatuses.done.index);
    }

    isAssigned() {
        if (!this.user) return false;

        return (this.task.assigned_id == this.user.id);
    }

    canReview() {
        return this.task.can_decline || this.task.can_approve;
    }

    canDecline() {
        return this.task.can_decline;
    }

    approve() {
        this.ProjectTaskService.approve(this.task.id);
    }

    decline() {
        this.ProjectTaskService.decline(this.task.id);
    }

    review() {
        this.ProjectTaskService.review(this.task.id);
    }
}

const TaskControls = {
    controller: TaskControlsComponentController,
    template: template,
    bindings: {
        task: '<'
    }
};

export default TaskControls;