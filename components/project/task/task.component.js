import template from './task.template.html';

class TaskComponentController {
    static $inject = [
        '$state',
        'ProjectTaskService'
    ];

    constructor($state, ProjectTaskService) {
        this.state = $state;
        this.ProjectTaskService = ProjectTaskService;
    }

    formatDealineDate() {
        return moment(this.task.deadline).format('D MMM, h:mm a');
    }

    chooseTask() {
        if (this.state.params.taskId == this.task.id) {
            this.state.go('pm.project.show.designer');
        } else {
            this.ProjectTaskService.activeTask = this.task;
            this.state.go('pm.project.show.designer.chat', {taskId: this.task.id})
        }
    }
}

const TaskComponent = {
    controller: TaskComponentController,
    template: template,
    bindings: {
        task: '<'
    }
};

export default TaskComponent;