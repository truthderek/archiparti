import HeaderDropdownComponentTemplate from './header-dropdown.template.html';

class HeaderDropdownComponentController {
    static $inject = [
        'ProjectEventFeedService',
        'ProjectEventService',
        'ProjectService'
    ];

    constructor(ProjectEventFeedService, ProjectEventService, ProjectService) {
        ProjectEventFeedService.subscribe(this);
        ProjectEventService.subscribe(this);

        this.ProjectEventService = ProjectEventService;
        this.ProjectService = ProjectService;

        this.unread = 0;

        this.loadEvents();
    }

    canKill() {
        return false;
    }

    notify(jsonEvent) {
        this.events = [JSON.parse(jsonEvent), this.events.shift()].filter(event => Boolean(event));
        this.unread++;
    }

    notifyEventRead() {
        this.loadEvents();
    }

    activeTask(task) {
        this.ProjectService.activeTask = task;
    }

    readEvent(eventId) {
        this.ProjectEventService.readByEvents([eventId]);
    }

    loadEvents() {
        this.ProjectEventService.dropdown()
            .then(response => {
                this.events = response.data.events.data;
                this.unread = response.data.events.total;
            });
    }

    formatDate(date) {
        return moment(date).fromNow();
    }
}

const HeaderDropdown = {
    controller: HeaderDropdownComponentController,
    template: HeaderDropdownComponentTemplate
};

export default HeaderDropdown;