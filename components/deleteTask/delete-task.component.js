import DeleteTaskComponentTemplate from './delete-task.template.html';

class DeleteTaskComponentController {
    static $inject = [
        'ProjectTaskService',
        '$state',
    ];

    constructor(ProjectTaskService, $state) {
        ProjectTaskService.subscribe(this);

        this.ProjectTaskService = ProjectTaskService;
        this.state = $state;

        this.tasks = this.ProjectTaskService.currentTasks;
        this.tasksToDelete = [];
    }

    toggleTask(id) {
        let index = this.findTask(id);

        if (index === -1) {
            this.tasksToDelete.push(id);
        } else {
            this.tasksToDelete.splice(index, 1);
        }
    }

    toggleTaskGroup(task) {
        let tasks = [];
        let checked = true;

        task.children.forEach(child => {
            let index = this.findTask(child.id);

            if (index == -1) {
                checked = false;
                return;
            } else {
                tasks.push(child);
            }
        });

        if (checked) {
            this.removeTasks(tasks);
        } else {
            this.addTasks(task.children);
        }
    }

    findTask(id) {
        return this.tasksToDelete.findIndex(task => task == id);
    }

    removeTasks(tasks) {
        tasks.forEach(task => {
            this.tasksToDelete.splice(this.findTask(task.id), 1);
        });
    }

    addTasks(tasks) {
        tasks.forEach(task => {
            if (this.findTask(task.id) == -1) {
                this.tasksToDelete.push(task.id);
            }
        });
    }

    notifyTasks(tasks) {
        this.tasks = tasks;
    }

    delete() {
        this.ProjectTaskService.delete(this.tasksToDelete)
            .then(response =>{
                $('.b-modal__close--confirm-delete-task').click();
                $('.b-modal__close--delete-task').click();
                this.state.go(this.state.current.name, this.state.params, {reload: true});
            });
    }
}

const DeleteTask = {
    controller: DeleteTaskComponentController,
    template: DeleteTaskComponentTemplate
};

export default DeleteTask;
