import Header from './header/header.component';
import Help from './help/help.component';
import HeaderDropdown from './header-dropdown/header-dropdown.component';
import Gallery from './gallery/gallery.component';
import Avatar from './avatar/avatar.component';
import Task from './project/task/task.component';
import TaskControls from './project/task-controls/task-controls.component';
import ContractLink from './project/contract/contract-link/contract-link.component';
import AddTask from './addTask/add-task.component';
import DeleteTask from './deleteTask/delete-task.component';

angular.module('Components', [])

.component('header', Header)
.component('help', Help)
.component('headerDropdown', HeaderDropdown)
.component('gallery', Gallery)
.component('avatar', Avatar)
.component('task', Task)
.component('taskControls', TaskControls)
.component('contractLink', ContractLink)
.component('addTask', AddTask)
.component('deleteTask', DeleteTask);
