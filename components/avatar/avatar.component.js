import AvatarComponentTemplate from './avatar.template.html';

class AvatarComponentController {
}

const Avatar = {
    controller: AvatarComponentController,
    template: AvatarComponentTemplate,
    bindings: {
        user: '<',
        size: '@',
        className: '@',
        title: '@',
        alt: '@'
    }
};

export default Avatar;