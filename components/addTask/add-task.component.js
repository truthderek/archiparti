import AddTaskComponentTemplate from './add-task.template.html';

class AddTaskComponentController {
    static $inject = [
        'ProjectService',
        'ProjectDesignerService',
        'ProjectTaskService',
        '$state',
        '$scope',
        '$window'
    ];

    constructor(ProjectService, ProjectDesignerService, ProjectTaskService, $state, $scope, $window) {
        ProjectService.subscribe(this);
        ProjectTaskService.subscribe(this);

        this.ProjectService = ProjectService;
        this.ProjectDesignerService = ProjectDesignerService;
        this.ProjectTaskService = ProjectTaskService;
        this.state = $state;
        this.window = $window;
        this.newGroup = {};
        this.task = {};
        this.users = [];
        this.colors = this.getColors();

        this.project = this.ProjectService.activeProject;
        this.tasks = this.ProjectTaskService.currentTasks;

        if (this.project) {
            this.getUsers();
        }
    }

    notifyActiveProject(project) {
        this.project = project;

        this.getUsers();
    }

    notifyTasks(tasks)
    {
        Object.assign(this.tasks, tasks);
    }

    getUsers() {
        this.ProjectDesignerService.list(this.project.id)
        .then(response => {
            this.users.push(response.data.designers.find(designer => designer.id == this.state.params.designerId));

            this.users.push(this.project.owner);
        });
    }

    save() {
        if ($('.b-form-add-task').find('.error').length) {
            return;
        }

        let designer = this.state.params.designerId;

        this.ProjectTaskService.add(this.project.id, designer, this.task)
            .then(response => {
                $('.b-modal__close--add-task').click();
                this.task = {};
                this.newGroup = {};
                this.window.location.reload();
            });
    }

    appendOption() {
        if (!this.newGroup.name) {
            return;
        }

        this.tasks[this.newGroup.name] = this.newGroup;
    }

    getColors() {
        return [
            '#e21212',
            '#73adb8',
            '#e24c12',
            '#12e23b',
            '#e21287',
            '#e2cd12',
            '#1c12e2',
            '#db12e2',
            '#b4e212',
            '#10a385',
            '#91c9bd',
            '#3a7c2c',
            '#e6c0e8',
            '#7a6953',
            '#634723',
            '#1c1102',
            '#ff4753',
            '#ffee00',
            '#661a05',
            '#96776f'
        ];
    }
}

const AddTask = {
    controller: AddTaskComponentController,
    template: AddTaskComponentTemplate
};

export default AddTask;